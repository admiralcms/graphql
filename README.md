# GraphQL
GraphQL plugin that allows everyone to have a GraphQL API up and running with their CakePHP application in little to no time!

## Installation
This plugin can be downloaded from [Packagist](https://packagist.org/packages/admiral/graphql) using Composer:
```
composer require admiral/graphql
```

## Documentation
You can find the full documentation in the [docs](/docs) folder of the distribution.  
If you don't know yet what GraphQL is, you should check out [the official website](https://graphql.org/)!

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | CakePHP Version |
|----------------|-----------------|
| 1.x            | 3.x             |