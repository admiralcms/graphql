<?php
  use Cake\Routing\Route\DashedRoute;
  use Cake\Routing\Router;

  /**
   * Routes for the admin dashboard
   */
  Router::plugin('Admiral/GraphQL', ['path' => '/graphql'],function ($routes) {
    $routes->connect('/', ['plugin' => 'Admiral/GraphQL', 'controller' => 'Graphql', 'action' => 'index', '_name' => 'graphql']);

    $routes->fallbacks(DashedRoute::class);
  });