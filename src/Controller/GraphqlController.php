<?php
  namespace Admiral\GraphQL\Controller;

  use Admiral\GraphQL\Types;
  use Admiral\GraphQL\Types\Definition\QueryType;
  use Admiral\GraphQL\Types\Definition\MutationType;

  use App\Controller\AppController;
  use Cake\Core\Configure;
  use Cake\Event\Event;
  use GraphQL\GraphQL;
  use GraphQL\Type\Schema;
  use GraphQL\Error\FormattedError;
  use GraphQL\Error\DebugFlag;
  use GraphQL\Language\Parser;
  use GraphQL\Utils\SchemaPrinter;

  class GraphqlController extends AppController {
    private $debug = false;
    private $schema;

    /**
     * Initialize the GraphQL by setting some flags
     */
    public function initialize(): void {
      // Initialize the parent
      parent::initialize();

      // Load required components
      $this->loadComponent('Admiral/GraphQL.Json');

      // Set debug flags if we're in debug mode
      if(Configure::read('debug') === true) $this->debug = DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE;

      // Register our QueryType
      // Then register our MutationType
      Types::register('query', (new QueryType())->config());
      Types::register('mutation', (new MutationType())->config());

      // Create a GraphQL schema
      $this->schema = new Schema([
        'query' => Types::get('query'),
        'mutation' => Types::get('mutation'),
      ]);
    }

    /**
     * Allow this controller without any authentication
     */
    public function beforeFilter(Event $event) {
      $this->Auth->allow();
    }

    /**
     * Execute the GraphQL
     */
    public function index(){
      // Check whether we need to get the data from the POST body or GET params
      switch(strtolower($this->request->env('REQUEST_METHOD'))) {
        case 'post':
          $raw = $this->request->getData();
          break;
        case 'get':
        default:
          // TODO: Implement getting from query
          $raw = '';
          break;
      }

      if(empty($raw)) {
        return $this->Json
          ->setStatus(400)
          ->setData([
            'errors' => [
              'Query cannot be empty!',
            ],
          ])
          ->sendResponse();      
      }

      try {
        // Parse the data
        $parsed = $this->parse($raw['query'], $raw['variables'] ?? []);

        // Execute our query
        $result = GraphQL::executeQuery(
          $this->schema,
          $parsed['query'],
          null,
          null,
          $parsed['variables']
        );

        // Return the results
        return $this->Json
          ->setData($result->toArray($this->debug))
          ->sendResponse();
      } catch (\Exception $e) {
        return $this->Json
          ->setStatus(400)
          ->setData([
            'errors' => [
              FormattedError::createFromException($e, $this->debug)
            ]
          ])
          ->sendResponse();      
      }
    }

    /**
     * Parse the query and variables
     * 
     * @param string $query The Query string
     * @param string $variables JSON of all variables
     * @return array 
     */
    private function parse(string $query = '', $variables = ''): array {
      $parsed = Parser::parse($query);

      return [
        'query' => $parsed,
        'variables' => (is_array($variables) ? $variables : json_decode($variables, 1))
      ];
    }
  }