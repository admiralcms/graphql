<?php
  namespace Admiral\GraphQL;

  use Admiral\GraphQL\Types\Definition\{
    QueryType,
    StatusType
  };

  use GraphQL\Type\Definition\{
    Type,
    ObjectType,
    InputObjectType,
    ListOfType,
    IntType,
    BooleanType,
  };
  use GraphQL\Type\Schema;
  use GraphQL\GraphQL;

  class Types {
    private static $hasDefaults = false;
    private static $types = [];
    private static $inputs = [];

    /**
     * Register a new type into the API
     * 
     * @param string $name The name of the type
     * @param any $config The definition of the type
     * @param bool $overwrite Whether to overwrite the type (dangerous!)
     */
    public static function register(string $name, $config, bool $overwrite = false): void {
      // Check if the type already exists
      // Then check if it should be overwritten or not
      if(array_key_exists($name, self::$types)) {
        // Throw an exception if we don't want to overwrite
        if(!$overwrite) throw new \Exception('Type "' . $name . '" is already registered!');

        // We do want to overwrite
        self::$types[$name] = is_array($config) ? new ObjectType($config) : $config;
        return;
      }

      // Type doesn't exist yet, register it
      self::$types[$name] = is_array($config) ? new ObjectType($config) : $config;
    }

    /**
     * Register a new input type into the API
     * 
     * @param string $name The name of the type
     * @param any $config The definition of the type
     * @param bool $overwrite Whether to overwrite the type (dangerous!)
     */
    public static function registerInput(string $name, $config, bool $overwrite = false): void {
      // Check if the type already exists
      // Then check if it should be overwritten or not
      if(array_key_exists($name, self::$inputs)) {
        // Throw an exception if we don't want to overwrite
        if(!$overwrite) throw new \Exception('Input type "' . $name . '" is already registered!');

        // We do want to overwrite
        self::$inputs[$name] = is_array($config) ? new InputObjectType($config) : $config;
        return;
      }

      // Type doesn't exist yet, register it
      self::$inputs[$name] = is_array($config) ? new InputObjectType($config) : $config;
    }
    
    /**
     * Get a type from our registered types
     * 
     * @param string $name The name of the type
     * @return Type
     */
    public static function get(string $name) {
      // Check if we need to register defaults first
      if(!self::$hasDefaults) self::defaults();

      // Make sure the type exists
      // If not, throw an exception
      if(!array_key_exists($name, self::$types)) throw new \Exception('Type "' . $name . '" does not exist!');
      
      // Type exists
      // Return it
      return self::$types[$name];
    }

    /**
     * Get an input type from our registered inputs
     * 
     * @param string $name The name of the type
     * @return Type
     */
    public static function input(string $name) {
      // Check if we need to register defaults first
      if(!self::$hasDefaults) self::defaults();

      // Make sure the type exists
      // If not, throw an exception
      if(!array_key_exists($name, self::$inputs)) throw new \Exception('Type "' . $name . '" does not exist!');

      // Type exists
      // Return it
      return self::$inputs[$name];
    }

    public static function debugGet() {
      dd(self::$types);
    }

    public static function debugInput() {
      dd(self::$inputs);
    }

    /**
     * Register some default types
     * 
     * @return void
     */
    public static function defaults() {
      self::register('int', Type::int());
      self::register('id', Type::id());
      self::register('string', Type::string());
      self::register('boolean', Type::boolean());
      self::register('Status', (new StatusType)->config());

      self::registerInput('int', Type::int());
      self::registerInput('id', Type::id());
      self::registerInput('string', Type::string());
      self::registerInput('boolean', Type::boolean());

      // Inform that we now have the defaults
      self::$hasDefaults = true;
    }

    /**
     * Special case listOf type
     * 
     * @param Type $type The type this can be a list of
     * @return listOf
     */
    public static function listOf($type) {
      return new ListOfType($type);
    }
  }