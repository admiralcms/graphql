<?php
  namespace Admiral\GraphQL\Types\Definition;

  class MutationType {
    private static $fields = [];

    public function config() {
      return [
        'name' => 'Mutation',
        'fields' => static::$fields,
      ];
    }

    public static function addField(string $name, array $definition) {
      static::$fields[$name] = $definition;
    }
  }
