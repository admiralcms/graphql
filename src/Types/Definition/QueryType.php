<?php
  namespace Admiral\GraphQL\Types\Definition;

  class QueryType {
    private static $fields = [];

    public function config() {
      return [
        'name' => 'Query',
        'fields' => static::$fields,
      ];
    }

    public static function addField(string $name, array $definition) {
      static::$fields[$name] = $definition;
    }
  }