<?php
  namespace Admiral\GraphQL\Types\Definition;

  use Admiral\GraphQL\Types;

  class StatusType {
    public function config() {
      return [
        'name' => 'Status',
        'fields' => function() {
          return [
            'success' => [
              'type' => Types::get('boolean'),
              'description' => 'Whether this request was a success',
            ],
            'message' => [
              'type' => Types::get('string'),
              'description' => 'Optional message of this request'
            ],
            'error' => [
              'type' => Types::get('string'),
              'description' => 'Error description of this request'
            ]
          ];
        }
      ];
    }
  }