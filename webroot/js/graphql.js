class GraphQL {
  endpoint;
  variables = {};
  query = `query{}`;

  /**
   * Construct this class
   * 
   * @param string The endpoint to which to send our query
   */
  constructor(endpoint = '/graphql') {
    this.endpoint = endpoint;
  }

  /**
   * Send our query to the server
   * Return the response JSON as a promise
   * 
   * @param string query 
   * @param {*} variables 
   */
  execute() {
    return fetch(this.endpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        query: this.query, 
        variables: this.variables
      })
    })
    .then(r => r.json())
  }

  /**
   * Set our query string
   * 
   * @param string query 
   * @return {GraphQL} The instance of this class
   */
  setQuery(query) {
    this.query = query;

    return this;
  }

  /**
   * Add a variable to our variables object
   * 
   * @param string key 
   * @param {*} value 
   * @return {GraphQL} The instance of this class
   */
  addVariable(key, value) {
    this.variables[key] = value;

    return this;
  }
}